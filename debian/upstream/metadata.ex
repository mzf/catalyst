# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/catalyst/issues
# Bug-Submit: https://github.com/<user>/catalyst/issues/new
# Changelog: https://github.com/<user>/catalyst/blob/master/CHANGES
# Documentation: https://github.com/<user>/catalyst/wiki
# Repository-Browse: https://github.com/<user>/catalyst
# Repository: https://github.com/<user>/catalyst.git
