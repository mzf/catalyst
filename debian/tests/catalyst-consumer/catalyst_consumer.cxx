#include <cstdlib>
#include <cstring>
#include <iostream>

#include <catalyst.hpp>

int main(int argc, char* argv[])
{
  conduit_cpp::Node init_node;
  init_node["catalyst_load/implementation"] = "debian";
  enum catalyst_status status = catalyst_initialize(conduit_cpp::c_node(&init_node));
  if (status != catalyst_status_ok)
  {
    std::cerr << "Initialize failure: " << status << std::endl;
    return EXIT_FAILURE;
  }

  conduit_cpp::Node about_node;
  status = catalyst_about(conduit_cpp::c_node(&about_node));
  if (status != catalyst_status_ok)
  {
    std::cerr << "About failure: " << status << std::endl;
    return EXIT_FAILURE;
  }

  if(about_node["catalyst/implementation"].as_string() != "debian")
  {
    std::cerr << "Wrong implementation in about: " << about_node["catalyst/implementation"].to_string() << std::endl;
    return EXIT_FAILURE;
  }

  conduit_cpp::Node exec_node;
  status = catalyst_execute(conduit_cpp::c_node(&exec_node));
  if (status != catalyst_status_ok)
  {
    std::cerr << "Execute failure: " << status << std::endl;
    return EXIT_FAILURE;
  }

  conduit_cpp::Node finalize_node;
  status = catalyst_about(conduit_cpp::c_node(&finalize_node));
  if (status != catalyst_status_ok)
  {
    std::cerr << "Finalize failure: " << status << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
