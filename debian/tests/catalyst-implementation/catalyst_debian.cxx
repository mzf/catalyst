#include <catalyst.hpp>
#include "catalyst_impl_debian.h"

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_initialize_debian(const conduit_node*)
{
  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_execute_debian(const conduit_node*)
{
  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_finalize_debian(const conduit_node*)
{
  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_results_debian(conduit_node*)
{
  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_about_debian(conduit_node* params)
{
  auto cpp_params = conduit_cpp::cpp_node(params);
  cpp_params["catalyst"]["capabilities"].append().set("debian");
  cpp_params["catalyst"]["implementation"] = "debian";

  return catalyst_status_ok;
}
