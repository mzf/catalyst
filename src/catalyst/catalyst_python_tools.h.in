/*
 * Distributed under OSI-approved BSD 3-Clause License. See
 * accompanying License.txt
 */
#ifndef catalyst_python_tools_h
#define catalyst_python_tools_h

// This header should be included only if an application uses python and it
// should be included *after* Python.h.

#cmakedefine01 CATALYST_WRAP_PYTHON

#if CATALYST_WRAP_PYTHON
// The condition checks indirectly if PyObject is already defined (due to
// e.g the consumers including Python.h themselves) in this case we do not want to
// redefine PyObject. see also https://docs.python.org/3/c-api/structures.html#c.PyObject_HEAD
#ifndef PyObject_HEAD
  #ifndef catalyst_python_tools_impl
  #error "Python.h needs to be included first"
  #else
  // forward declare PyObject
  struct _object;
  typedef _object PyObject;
  #endif
#endif
using catalyst_py_object = PyObject;
#else
using catalyst_py_object = struct
{
  int x;
};
#endif

// include conduit C API.
#include <conduit.h>

#include <catalyst_export.h>

#ifdef __cplusplus
extern "C"
{
#endif
  // Wrap a conduit node for python. This is indented for passing a node from
  // C/C++ to python if owns == 1 the ownership of the node passes to Python
  // and thus upon descrution of the PyObject the node is freed.
  CATALYST_EXPORT catalyst_py_object* PyCatalystConduit_Node_Wrap(conduit_node* node, int owns);
#ifdef __cplusplus
} // extern "C"
#endif

#endif // #ifndef catalyst_python_tools_h
